# Aesop's Fables in Láadan

This is meant as a practice in writing a book for self-publishing.
I would like to eventually publish a new grammar book and dictionary for Láadan,
but I've never self-published before, so a low-importance item seems like the best idea.
